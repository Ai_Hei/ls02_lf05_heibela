package Spaceship;

import java.util.ArrayList;

public class Raumschiff {

	private String name;
	private int powersupplyPer;
	private int photonentorpedoCount;
	private int shieldsPer;
	private int shellPer;
	private int lifeSupportSysPer;
	private int repAndroids;
	private int hitCount;
	/*private int phasercanon;
	private int tricorderCount;
	private int probeCount; */
	private static ArrayList<String> broadcastCommunicator;
	private ArrayList<Ladung> cargo;
	private ArrayList<String> logEntries;

	// Methods

	public Raumschiff() {
	}

	
	public Raumschiff(String name, int powersupplyPer, int photonentorpedoCount, int shieldsPer, int shellPer,
			int lifeSupportSysPer, int repAndroids, ArrayList<String> logEntries) {
		setName(name);
		setPowersupplyPer(powersupplyPer);
		setPhotonentorpedoCount(photonentorpedoCount);
		setShieldsPer(shieldsPer);
		setShellPer(shellPer);
		setLifeSupportSysPer(lifeSupportSysPer);
		setRepAndroids(repAndroids);
		//setHitCount(hitCount);
		this.logEntries = new ArrayList<String>();
		Raumschiff.broadcastCommunicator = new ArrayList<String>();
		this.cargo = new ArrayList<Ladung>();

	}

	

	// Getters and Setters

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPowersupplyPer() {
		return powersupplyPer;
	}

	public void setPowersupplyPer(int powersupplyPer) {
		this.powersupplyPer = powersupplyPer;
	}

	public int getPhotonentorpedoCount() {
		return photonentorpedoCount;
	}

	public void setPhotonentorpedoCount(int photonentorpedoCount) {
		this.photonentorpedoCount = photonentorpedoCount;
	}

	public int getShieldsPer() {
		return shieldsPer;
	}

	public void setShieldsPer(int shieldsPer) {
		this.shieldsPer = shieldsPer;
	}

	public int getShellPer() {
		return shellPer;
	}

	public void setShellPer(int shellPer) {
		this.shellPer = shellPer;
	}

	public int getLifeSupportSysPer() {
		return lifeSupportSysPer;
	}

	public void setLifeSupportSysPer(int lifeSupportSysPer) {
		this.lifeSupportSysPer = lifeSupportSysPer;
	}

	public int getRepAndroids() {
		return repAndroids;
	}

	public void setRepAndroids(int repAndroids) {
		this.repAndroids = repAndroids;
	}

	public int getHitCount() {
		return hitCount;
	}

	public void setHitCount(int hitCount) {
		this.hitCount = hitCount;
	}

	public static ArrayList<String> getBroadcastCommunicator() {
		return broadcastCommunicator;
	}

	public static void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
		Raumschiff.broadcastCommunicator = broadcastCommunicator;
	}

	public ArrayList<String> getLogEntries() {
		return logEntries;
	}

	public void setLogEntries(ArrayList<String> logEntries) {
		this.logEntries = logEntries;
	}

	
	
	// Adding Items to your Cargo
	public ArrayList<Ladung> getCargo() {
		return this.cargo;
	}

	public void setCargo(Ladung cargo) {
		this.cargo.add(cargo);
	}
	
	public void addCargo(Ladung items) {
		this.cargo.add(items);
	}

	
	// Show the condition of the Spaceship
	public void showCondotion() {
		System.out.printf("The Spaceship: %s ", this.name);
		System.out.printf("Powersupplies condition: %d ", this.powersupplyPer);
		System.out.printf("Life Support Systems conditin: %d ", lifeSupportSysPer);
		System.out.printf("Shields conditin: %d ", this.shieldsPer);
		System.out.printf("Shells condition: %d ", this.shellPer);
		// show the cargos items?
	}
	
	
 // Get the Logs entries and add a new entry to the Communicator/Logs
	public ArrayList<String> showLog() {
		return this.logEntries;
	}
	public void sendMessage(String message) {
		Raumschiff.broadcastCommunicator.add(message);
		System.out.println("The message" + message + "has been added to the broadcast communicator.");
	}

	
	//Deletes empty Items from the CargoList
	public void emptyCargo() {
		for (int i = 0; i < this.cargo.size(); i++) {
			if (this.cargo.get(i).getQuantity() == 0) {
				System.out.println(this.cargo.get(i).getType());
				this.cargo.remove(i);
				
			}
		}
	}

	
	//Register a hit and set the Ships status bars accordingly
	private void hit(Raumschiff Ship) {
		System.out.println("" + Ship.name + "has been hit");
		if (Ship.shieldsPer > 49) {
			Ship.shieldsPer = -50;
		} else {
			Ship.shellPer = -50;
			Ship.powersupplyPer = -50;
			if (Ship.shellPer < 0) {
				System.out.println("" + Ship.name + "Life Support Systems have been destroyed!");
				Ship.lifeSupportSysPer = 0;
			}
		}
	}

	
	// Using attacks and setting the Spaceshios conditions accordingly
	public void shootPhotonentorpedo(Raumschiff Ship) {
		if (this.photonentorpedoCount > 0) {
			System.out.println("Photonentorpedo has been shot");
			hit(Ship);
			this.photonentorpedoCount = -1;
		} else {
			System.out.println(" -=*Click*=- ");
		}
	}
	public void shootPhasercanon(Raumschiff Ship) {
		if (this.powersupplyPer > 49) {
			System.out.println("Phasercanon has been shot");
			hit(Ship);
			this.powersupplyPer = -50;
		} else {
			System.out.println(" -=*Click*=- ");
		}

	}

	
	// Gets the cargos information and shows all items that are still available
	public void statusCargo() {
		System.out.println("" + this.name + "has ");
		for (int i = 0; i < this.cargo.size(); i++)
			System.out.println(this.cargo.get(i).toString());
		System.out.println("items in the cargo hold.");
	}
	
	
	// Sends out the androids to repair the damaged areas of a ship
	public void repairShip(boolean shields, boolean shell, boolean powersupply, 
			int repAndroids) {
		if(repAndroids <= this.repAndroids) {
			System.out.println(this.name + "is being repaired.");
			int coincidence;
			int taskCompletion;
			coincidence = (int)(Math.random()*100);
			
			if(shields & shell & powersupply) {
				taskCompletion = coincidence * repAndroids / 3;
				this.shellPer = + taskCompletion;
				this.powersupplyPer = + taskCompletion;
				this.shieldsPer = + taskCompletion;
			}
			else if (shell & shields) {
				taskCompletion = coincidence * repAndroids / 2;
				this.shellPer = + taskCompletion;
				this.shieldsPer = + taskCompletion;
			}
			else if (shell & powersupply) {
				taskCompletion = coincidence * repAndroids / 2;
				this.shellPer = + taskCompletion;
				this.powersupplyPer = + taskCompletion;
			}
			else if (shields & powersupply) {
				taskCompletion = coincidence * repAndroids / 2;
				this.shieldsPer = + taskCompletion;
				this.powersupplyPer = + taskCompletion;
			}
			else if (shell) {
				taskCompletion = coincidence * repAndroids;
				this.shellPer = + taskCompletion;
			}
			else if (shields) {
				taskCompletion = coincidence * repAndroids;
				this.shieldsPer = + taskCompletion;
			}
			else if (powersupply) {
				taskCompletion = coincidence * repAndroids;
				this.powersupplyPer = + taskCompletion;
			}
		else {
			System.out.println("There aren't enough Androids on board to complete this task!");
			
		}
		}
	}

}
