package Spaceship;


public class MainRaumschiff {

	public static void main(String[] args) {
		
		//Creating the "Spaceship" objects
		Raumschiff vulkanier = new Raumschiff("Ni'Var", 80, 3, 80, 50, 100, 5, null);
		Raumschiff romulaner = new Raumschiff("IRK Khazara", 50, 2, 50, 100, 100, 2, null);
		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 0, 50, 100, 100, 2, null);
	
		
		//Items that can be added to the Cargo
		Ladung juice = new Ladung("Ferengi-Schneckensaft", 200);
		Ladung sword = new Ladung("Bat'Leth-Klingonen-Schwert", 200);
		Ladung trash = new Ladung("Borg-Schrott", 5);
		Ladung weapon = new Ladung("Plasma-Waffe", 50);
		Ladung weird = new Ladung("Rote-Materie", 2);
		Ladung probes = new Ladung("Forschungssonde", 35);
		Ladung torpedo = new Ladung("Photonentorpedo", 3);
		
		
		// Adding items to the cargo of a specific Spaceship
		vulkanier.setCargo(torpedo);
		vulkanier.setCargo(probes);
		romulaner.setCargo(weird);
		romulaner.setCargo(trash);
		romulaner.setCargo(weapon);
		klingonen.setCargo(juice);
		klingonen.setCargo(sword);		
	
		//Attacks
		romulaner.shootPhasercanon(klingonen);
		klingonen.shootPhotonentorpedo(romulaner);
		
	}

}
