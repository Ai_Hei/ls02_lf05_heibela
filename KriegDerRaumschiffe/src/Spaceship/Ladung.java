package Spaceship;

/**
 * This class is modeled to be a Cargo
 * Fortgeschrittene?
 * @author Aylin Heibel
 * @version xx.03.2020
 */

public class Ladung {

	private String type;
	private int quantity = 0;
	
	/** Parameterless constructor of the class Ladung */
	public Ladung() {
	}
	
	/** Constructor for the class Ladung, 
	 * @param type defines the type of item added
	 * @param quantity defines the quantity of the item added
	 */ 
	public Ladung(String type, int quantity) {
		setType(type);
		setQuantity(quantity);
	}
	
	
	//Changes the type of the meth.
	@Override
	public String toString() {
		return "Item-Type: " + type + ", Item-Quantity: "+ quantity;
	}
	
	
  /** Constructor to delete items of the cargo list when their quantity has reached 0 */
	public void emptyCargo() {
		this.type = null;
		this.quantity = 0;
	}
	
	
	//Getter and Setter
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
	
}
