package Rechteck;

public class Rechteck {
private double a; 
private double b;

public Rechteck(double a, double b) {
setA(a); 
setB(b);
}

public void setA(double a) {
	if(a > 0)
	this.a = a; 
	else
	this.a = 0;
}

public double getA() {
	return this.a;
}

public void setB(double b) {
	if(b > 0)
	this.b = b;
	else 
		this.b = 0;
}

public double  getB() {
	return this.b;
}

public double getArea() {
	return a*b;
}

public double getPerimeter() {
	return 2*a + 2*b;
}

/* public double diagonal() {
 } 
 */
}
